Entity Editor Tabs

Initial development and maintenance by PreviousNext.

https://www.drupal.org/project/entity_editor_tabs

# Usage

 1. Install the module as per usual.
 2. Enable Layout Builder and/or Content Moderation and configure each for an
    entity bundle.
 3. Create and save an entity. Navigate to the View page of the entity. You'll
    see Drupal's tabs are customized.

# Recommendation

Consider using the [Layout Builder Operation Link](https://www.drupal.org/project/layout_builder_operation_link)
to add an _Edit_ item from entity lists directly to Layout Builder.

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
