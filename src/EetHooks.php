<?php

declare(strict_types=1);

namespace Drupal\entity_editor_tabs;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\entity_editor_tabs\Plugin\Menu\LocalTask\EetCanonicalLocalTask;
use Drupal\entity_editor_tabs\Plugin\Menu\LocalTask\EetLatestLocalTask;
use Drupal\entity_editor_tabs\Plugin\Menu\LocalTask\EetUpdateLocalTask;
use Drupal\entity_route_context\EntityRouteContextRouteHelperInterface;

/**
 * Hooks for Entity Editor Tabs.
 */
final class EetHooks {

  use StringTranslationTrait;

  /**
   * Constructs a new EetHooks.
   */
  public function __construct(
    protected EntityTypeBundleInfoInterface $bundleInfo,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EetUtility $eetUtility,
    protected EntityRouteContextRouteHelperInterface $routeHelper,
    protected ?ModerationInformationInterface $moderationInformation,
    protected ModuleHandlerInterface $moduleHandler,
    TranslationInterface $translation,
  ) {
    $this->setStringTranslation($translation);
  }

  /**
   * Implements hook_local_tasks_alter().
   *
   * @see \entity_editor_tabs_local_tasks_alter()
   */
  public function hookLocalTasksAlter(array &$localTasks): void {
    foreach ($this->bundles() as [$entityTypeId, $bundle]) {
      if (!$this->moduleHandler->moduleExists('layout_builder') && !$this->moduleHandler->moduleExists('content_moderation')) {
        continue;
      }

      $canonicalLocalTaskId = $this->getLocalTaskIdFromLinkTemplate($entityTypeId, 'canonical', $localTasks);
      $editFormLocalTaskId = $this->getLocalTaskIdFromLinkTemplate($entityTypeId, 'edit-form', $localTasks);
      $latestVersionLocalTaskId = $this->getLocalTaskIdFromLinkTemplate($entityTypeId, 'latest-version', $localTasks);
      $deleteFormLocalTaskId = $this->getLocalTaskIdFromLinkTemplate($entityTypeId, 'delete-form', $localTasks);
      // From \Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage::buildLocalTasks:
      $overrideTaskId = \sprintf('layout_builder_ui:layout_builder.overrides.%s.view', $entityTypeId);
      $overrideTaskId = isset($localTasks[$overrideTaskId]) ? $overrideTaskId : NULL;

      // Re-order tasks so view and edit tasks are respectively together.
      // This strategy attempts to keep the original weight of each task if
      // they are already in the correct order.
      // Tasks and their expected order.
      $tasks = [
        $canonicalLocalTaskId,
        $latestVersionLocalTaskId,
        $editFormLocalTaskId,
        $overrideTaskId,
        $deleteFormLocalTaskId,
      ];
      // Remove non existent:
      $tasks = \array_filter($tasks);

      $lastWeight = (int) ($localTasks[$canonicalLocalTaskId]['weight'] ?? NULL);
      // Never modify canonical weight.
      \array_shift($tasks);
      foreach ($tasks as $taskId) {
        $taskWeight = (int) ($localTasks[$taskId]['weight'] ?? NULL);
        if ($taskWeight <= $lastWeight) {
          $localTasks[$taskId]['weight'] = $lastWeight + 1;
        }
        $lastWeight = $localTasks[$taskId]['weight'];
      }

      if ($this->moduleHandler->moduleExists('layout_builder') && $this->eetUtility->isLayoutBuilderOverridable($entityTypeId, $bundle)) {
        // 'Edit' tab override:
        if ($editFormLocalTaskId !== NULL) {
          $localTasks[$editFormLocalTaskId]['class'] = EetUpdateLocalTask::class;
        }

        // 'Layout' tab override:
        if ($overrideTaskId !== NULL) {
          $localTasks[$overrideTaskId]['title'] = \t('Edit content');
        }
      }

      if ($this->moduleHandler->moduleExists('content_moderation') && $this->moderationInformation->shouldModerateEntitiesOfBundle($this->entityTypeManager->getDefinition($entityTypeId), $bundle)) {
        // 'View' tab override:
        if ($canonicalLocalTaskId !== NULL) {
          $localTasks[$canonicalLocalTaskId]['class'] = EetCanonicalLocalTask::class;
        }

        // 'Latest version' tab override:
        if ($latestVersionLocalTaskId !== NULL) {
          $localTasks[$latestVersionLocalTaskId]['class'] = EetLatestLocalTask::class;
        }
      }
    }
  }

  /**
   * Implements hook_entity_operation_alter().
   *
   * @param array<array{title:string,url:\Drupal\Core\Url,weight:int}> $operations
   *   Existing entity operations.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @see \entity_editor_tabs_entity_operation_alter()
   */
  public function hookEntityOperationAlter(array &$operations, EntityInterface $entity): void {
    if (!$this->moduleHandler->moduleExists('layout_builder') || !$this->eetUtility->isLayoutBuilderOverridable($entity->getEntityTypeId(), $entity->bundle())) {
      return;
    }

    $routes = $this->routeHelper->getRouteNames($entity->getEntityTypeId());
    $updateRouteName = $routes['edit-form'] ?? NULL;
    $lbRouteName = \sprintf('layout_builder.overrides.%s.view', $entity->getEntityTypeId());

    foreach ($operations as ['url' => $url, 'title' => &$title]) {
      switch ($url->getRouteName()) {
        case $updateRouteName:
          $title = $this->t('Edit metadata', []);
          break;

        case $lbRouteName:
          $title = $this->t('Edit @singular', [
            // This will show 'content item' for Node, etc.
            '@singular' => $entity->getEntityType()->getSingularLabel(),
          ]);
          break;
      }
    }
  }

  /**
   * Generates all bundles in the system as a flat series of arrays.
   *
   * @phpstan-return \Generator<array{string, string}>
   */
  protected function bundles(): \Generator {
    foreach ($this->bundleInfo->getAllBundleInfo() as $entityTypeId => $info) {
      foreach (\array_keys($info) as $bundle) {
        yield [$entityTypeId, $bundle];
      }
    }
  }

  /**
   * Finds the first local task representing a link template.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string $linkTemplate
   *   The link template.
   * @param array $localTasks
   *   Local task definitions.
   *
   * @return string|null
   *   The local task ID in $localTasks, or NULL if no task was found.
   */
  protected function getLocalTaskIdFromLinkTemplate(string $entityTypeId, string $linkTemplate, array $localTasks): ?string {
    $routes = $this->routeHelper->getRouteNames($entityTypeId);
    $routeName = $routes[$linkTemplate] ?? NULL;
    foreach ($localTasks as $id => $definition) {
      if (($definition['route_name'] ?? NULL) === $routeName) {
        return $id;
      }
    }
    return NULL;
  }

}
