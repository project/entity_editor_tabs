<?php

declare(strict_types=1);

namespace Drupal\entity_editor_tabs;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Utility for Entity Editor Tabs.
 */
final class EetUtility {

  /**
   * EetUtility constructor.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * Determines whether a bundle has individual entity layout overrides enabled.
   *
   * @param string $entityTypeId
   *   An entity type ID.
   * @param string $bundle
   *   A bundle machine name.
   *
   * @return bool
   *   Whether a bundle has individual entity layout overrides enabled.
   */
  public function isLayoutBuilderOverridable(string $entityTypeId, string $bundle): bool {
    $storage = $this->entityTypeManager->getStorage('entity_view_display');
    $count = $storage->getQuery()
      ->condition('targetEntityType', $entityTypeId)
      ->condition('bundle', $bundle)
      ->condition('third_party_settings.layout_builder.allow_custom', TRUE)
      ->count()
      ->execute();
    return $count > 0;
  }

}
