<?php

declare(strict_types=1);

namespace Drupal\entity_editor_tabs\Plugin\Menu\LocalTask;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\entity_route_context\EntityRouteContextRouteHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a local task with customization when content moderation is enabled.
 */
final class EetLatestLocalTask extends LocalTaskDefault implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use EetLocalTaskTrait;

  /**
   * Constructs a new EetLatestLocalTask.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ModerationInformationInterface $moderationInformation,
    RouteMatchInterface $routeMatch,
    ContextRepositoryInterface $contextRepository,
    EntityRouteContextRouteHelperInterface $entityRouteHelper,
    TranslationInterface $translation,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->contextRepository = $contextRepository;
    $this->entityRouteHelper = $entityRouteHelper;
    $this->setStringTranslation($translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('content_moderation.moderation_information'),
      $container->get('current_route_match'),
      $container->get('context.repository'),
      $container->get('entity_route_context.route_helper'),
      $container->get('string_translation'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(?Request $request = NULL) {
    $entity = $this->getEntity();
    if ($entity === NULL) {
      return parent::getTitle($request);
    }

    if (!$this->moderationInformation->shouldModerateEntitiesOfBundle($this->entityTypeManager->getDefinition($entity->getEntityTypeId()), $entity->bundle())) {
      return parent::getTitle($request);
    }

    if ($this->moderationInformation->isLiveRevision($entity)) {
      // No customization if there is unpublished forward revision.
      // @todo make this configurable.
      return parent::getTitle($request);
    }

    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $latestRevisionId = $storage->getLatestRevisionId($entity->id());
    if ($latestRevisionId === NULL) {
      return parent::getTitle($request);
    }

    // We know it exists, per above.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $latestRevision */
    $latestRevision = $storage->loadRevision($latestRevisionId);
    $state = $this->moderationInformation->getOriginalState($latestRevision);

    // TranslatableMarkup is actually allowed.
    // @phpstan-ignore-next-line
    return $this->t('View Latest @state', [
      '@state' => $state->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $entity = $this->getEntity();
    return \array_merge(
      parent::getCacheTags(),
      ($entity?->getCacheTags() ?? []),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'route';
    return $contexts;
  }

}
