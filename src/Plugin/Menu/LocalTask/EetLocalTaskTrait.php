<?php

declare(strict_types=1);

namespace Drupal\entity_editor_tabs\Plugin\Menu\LocalTask;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_route_context\EntityRouteContextRouteHelperInterface;

/**
 * Common utilities for local tasks.
 */
trait EetLocalTaskTrait {

  /**
   * The entity for this route.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|null|false
   *   The entity for this route, or NULL if has not been determined, or FALSE
   *   if there is no entity for this route.
   */
  protected $routeEntity = NULL;
  protected RouteMatchInterface $routeMatch;
  protected ContextRepositoryInterface $contextRepository;
  protected EntityRouteContextRouteHelperInterface $entityRouteHelper;

  /**
   * Get entity for this route.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity for this route.
   */
  protected function getEntity(): ?ContentEntityInterface {
    if ($this->routeEntity === FALSE) {
      return NULL;
    }

    if ($this->routeEntity instanceof ContentEntityInterface) {
      return $this->routeEntity;
    }

    $contextName = '@entity_route_context.entity_route_context:canonical_entity';
    $contexts = $this->contextRepository->getRuntimeContexts([$contextName]);
    $entity = isset($contexts[$contextName]) ? $contexts[$contextName]->getContextValue() : NULL;
    if ($entity !== NULL) {
      return $this->routeEntity = $entity;
    }

    // Getting the entity via base route is the next most reliable method since
    // the base route is typically entity.ENTITY_TYPE_ID.canonical, which is
    // usually registered as a link template.
    $entityTypeId = $this->entityRouteHelper->getEntityTypeId($this->pluginDefinition['base_route']);
    $parameter = $this->routeMatch->getParameter($entityTypeId);
    if ($parameter instanceof ContentEntityInterface) {
      return $this->routeEntity = $parameter;
    }
    else {
      $this->routeEntity = FALSE;
      return NULL;
    }
  }

}
