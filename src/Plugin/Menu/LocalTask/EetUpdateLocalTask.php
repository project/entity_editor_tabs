<?php

declare(strict_types=1);

namespace Drupal\entity_editor_tabs\Plugin\Menu\LocalTask;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\entity_editor_tabs\EetUtility;
use Drupal\entity_route_context\EntityRouteContextRouteHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a local task with customization when layout builder is enabled.
 */
final class EetUpdateLocalTask extends LocalTaskDefault implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use EetLocalTaskTrait;

  /**
   * Constructs a new EetLatestLocalTask.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EetUtility $eetUtility,
    RouteMatchInterface $routeMatch,
    ContextRepositoryInterface $contextRepository,
    EntityRouteContextRouteHelperInterface $entityRouteHelper,
    TranslationInterface $translation,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->contextRepository = $contextRepository;
    $this->entityRouteHelper = $entityRouteHelper;
    $this->setStringTranslation($translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get(EetUtility::class),
      $container->get('current_route_match'),
      $container->get('context.repository'),
      $container->get('entity_route_context.route_helper'),
      $container->get('string_translation'),
    );
  }

  public function getTitle(?Request $request = NULL): string {
    $entity = $this->getEntity();
    if (NULL === $entity) {
      return parent::getTitle($request);
    }

    if (!$this->eetUtility->isLayoutBuilderOverridable($entity->getEntityTypeId(), $entity->bundle())) {
      return parent::getTitle($request);
    }

    return (string) $this->t('Edit metadata', [], [
      'context' => 'layout_builder_overridable',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $entity = $this->getEntity();
    return \array_merge(
      parent::getCacheTags(),
      ($entity?->getCacheTags() ?? []),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'route';
    return $contexts;
  }

}
