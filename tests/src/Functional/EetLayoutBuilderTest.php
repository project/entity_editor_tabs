<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_editor_tabs\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Url;
use Drupal\entity_test\Entity\EntityTestMulRevPub;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests Layout Builder integrations with Entity Editor Tabs.
 *
 * @group entity_editor_tabs
 */
final class EetLayoutBuilderTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'entity_editor_tabs_test',
    'entity_editor_tabs',
    'entity_test',
    'block',
    'layout_builder',
    'node',
  ];

  protected UserInterface $testUser;

  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('local_tasks_block');
    $this->testUser = $this->drupalCreateUser([
      'view test entity',
      'administer entity_test content',
      'configure any layout',
    ]);
  }

  /**
   * Tests tabs are customized.
   */
  public function testLayoutBuilderTabs(): void {
    $display = LayoutBuilderEntityViewDisplay::create([
      'targetEntityType' => 'entity_test_mulrevpub',
      'bundle' => 'entity_test_mulrevpub',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $display
      ->enableLayoutBuilder()
      ->setOverridable()
      ->save();

    $this->drupalLogin($this->testUser);

    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
      'user_id' => $this->testUser,
    ]);
    $entity->save();

    $this->drupalGet($entity->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('View');
    $this->assertSession()->linkExists('Edit metadata');
    $this->assertSession()->linkExists('Edit content');
  }

  /**
   * Tests entity operations are customized.
   *
   * Drupal core does not provide a Layout Builder operation, instead we try to
   * detect and modify operations added via contrib or custom code.
   */
  public function testLayoutBuilderEntityOperations(): void {
    $this->drupalLogin($this->testUser);

    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
      'user_id' => $this->testUser,
    ]);
    $entity->save();

    $collectionUrl = Url::fromRoute('entity.entity_test_mulrevpub.collection');
    $this->drupalGet($collectionUrl);

    $as = $this->assertSession();
    $as->linkByHrefExists('/entity_test_mulrevpub/manage/1/edit');
    $as->linkByHrefNotExists('/entity_test_mulrevpub/manage/1/layout');
    $as->linkExistsExact('Edit');
    $as->linkNotExistsExact('Edit metadata');
    $as->linkNotExistsExact('Edit mulrevpub item');

    $display = LayoutBuilderEntityViewDisplay::create([
      'targetEntityType' => 'entity_test_mulrevpub',
      'bundle' => 'entity_test_mulrevpub',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $display
      ->enableLayoutBuilder()
      ->setOverridable()
      ->save();

    Cache::invalidateTags(['rendered']);
    $this->drupalGet($collectionUrl);
    $as->linkByHrefExists('/entity_test_mulrevpub/manage/1/edit');
    $as->linkByHrefNotExists('/entity_test_mulrevpub/manage/1/layout');
    $as->linkNotExistsExact('Edit');
    $as->linkExistsExact('Edit metadata');
    $as->linkNotExistsExact('Edit mulrevpub item');

    // Add some code which adds a Layout tab.
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $moduleInstaller */
    $moduleInstaller = \Drupal::service(ModuleInstallerInterface::class);
    $moduleInstaller->install(['layout_builder_operation_link']);

    Cache::invalidateTags(['rendered']);
    $this->drupalGet($collectionUrl);
    $as->linkByHrefExists('/entity_test_mulrevpub/manage/1/edit');
    $as->linkByHrefExists('/entity_test_mulrevpub/manage/1/layout');
    $as->linkNotExistsExact('Edit');
    $as->linkExistsExact('Edit metadata');
    $as->linkExistsExact('Edit mulrevpub item');
  }

}
