<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_editor_tabs\Functional;

use Drupal\entity_test\Entity\EntityTestMulRevPub;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\user\UserInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Tests all integrations with Entity Editor Tabs.
 *
 * @group entity_editor_tabs
 */
final class EetTest extends BrowserTestBase {

  use ContentModerationTestTrait;

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'entity_editor_tabs_test',
    'entity_editor_tabs',
    'entity_test',
    'content_moderation',
    'workflows',
    'block',
    'layout_builder',
    'node',
  ];

  protected WorkflowInterface $testWorkflow;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser;

  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('local_tasks_block');
    $this->testWorkflow = $this->createEditorialWorkflow();
    $this->addEntityTypeAndBundleToWorkflow($this->testWorkflow, 'entity_test_mulrevpub', 'entity_test_mulrevpub');
    $this->testUser = $this->drupalCreateUser([
      'view test entity',
      'configure any layout',
      'view latest version',
      'view any unpublished content',
      'use ' . $this->testWorkflow->id() . ' transition create_new_draft',
      'use ' . $this->testWorkflow->id() . ' transition archive',
      'use ' . $this->testWorkflow->id() . ' transition publish',
      // Required by delete operation:
      'administer entity_test content',
    ]);

    $display = LayoutBuilderEntityViewDisplay::create([
      'targetEntityType' => 'entity_test_mulrevpub',
      'bundle' => 'entity_test_mulrevpub',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $display
      ->enableLayoutBuilder()
      ->setOverridable()
      ->save();

    $this->drupalLogin($this->testUser);
  }

  /**
   * Tests local task order.
   */
  public function testTabOrder(): void {
    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
      'user_id' => $this->testUser,
      'moderation_state' => 'published',
    ]);
    $entity->save();
    $firstRevisionId = $entity->getRevisionId();

    // @phpstan-ignore-next-line
    $entity->moderation_state = 'draft';
    $entity->setNewRevision();
    $entity->save();
    $secondRevisionId = $entity->getRevisionId();
    static::assertNotEquals($firstRevisionId, $secondRevisionId);

    $this->drupalGet($entity->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementTextContains('css', 'ul li:nth-child(1) a', 'View Published');
    $this->assertSession()->elementTextContains('css', 'ul li:nth-child(2) a', 'View Latest Draft');
    $this->assertSession()->elementTextContains('css', 'ul li:nth-child(3) a', 'Edit metadata');
    $this->assertSession()->elementTextContains('css', 'ul li:nth-child(4) a', 'Edit content');
    $this->assertSession()->elementTextContains('css', 'ul li:nth-child(5) a', 'Delete');
  }

}
