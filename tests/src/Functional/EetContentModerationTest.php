<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_editor_tabs\Functional;

use Drupal\entity_test\Entity\EntityTestMulRevPub;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\user\UserInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Tests Content Moderation integrations with Entity Editor Tabs.
 *
 * @group entity_editor_tabs
 */
final class EetContentModerationTest extends BrowserTestBase {

  use ContentModerationTestTrait;

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'entity_editor_tabs_test',
    'entity_editor_tabs',
    'content_moderation',
    'workflows',
    'entity_test',
    'block',
  ];

  protected WorkflowInterface $testWorkflow;
  protected UserInterface $testUser;

  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('local_tasks_block');
    $this->testWorkflow = $this->createEditorialWorkflow();
    $this->addEntityTypeAndBundleToWorkflow($this->testWorkflow, 'entity_test_mulrevpub', 'entity_test_mulrevpub');
    $this->testUser = $this->drupalCreateUser([
      'view test entity',
      'administer entity_test content',
      'view latest version',
      'view any unpublished content',
      'use ' . $this->testWorkflow->id() . ' transition create_new_draft',
      'use ' . $this->testWorkflow->id() . ' transition archive',
      'use ' . $this->testWorkflow->id() . ' transition publish',
    ]);

    $this->drupalLogin($this->testUser);
  }

  /**
   * Tests when entity doesn't have any published revision yet.
   */
  public function testTabsOnlyUnpublished(): void {
    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
      'user_id' => $this->testUser,
    ]);
    $entity->save();

    $this->drupalGet($entity->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('View Draft');
    $this->assertSession()->linkExists('Edit');
  }

  /**
   * Tests when entity has a published revision but no forward revisions.
   */
  public function testTabsOnlyPublished(): void {
    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
      'user_id' => $this->testUser,
      'moderation_state' => 'published',
    ]);
    $entity->save();

    $this->drupalGet($entity->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('View');
    $this->assertSession()->linkExists('Edit');
  }

  /**
   * Tests when entity has a published revision and forward revisions.
   */
  public function testPublishedWithForward(): void {
    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
      'user_id' => $this->testUser,
      'moderation_state' => 'published',
    ]);
    $entity->save();
    $firstRevisionId = $entity->getRevisionId();

    // @phpstan-ignore-next-line
    $entity->moderation_state = 'draft';
    $entity->setNewRevision();
    $entity->save();
    $secondRevisionId = $entity->getRevisionId();
    static::assertNotEquals($firstRevisionId, $secondRevisionId);

    $this->drupalGet($entity->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('View Published');
    $this->assertSession()->linkExists('View Latest Draft');
    $this->assertSession()->linkExists('Edit');
  }

}
